/**
 * @file bst.h
 * @author Sikny
 * simple Binary Search Tree class of values of type T. T needs
 * operator==, operator<, operator>
 */

#ifndef BST_H
#define BST_H

#include <string>

template <typename T>
class BST
{
public:
    BST();
    void add(T value);
    bool contains(T value);
    std::string toString();

protected:
    BST leftTree;
    BST rightTree;
    T value;
};

#endif // BST_H
