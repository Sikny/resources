#ifndef DICTIONARY_H
#define DICTIONARY_H

#include <string>
#include <iostream>

class Dictionary {
	public:
	    enum{LENGTH = 26};
        Dictionary();
        void add(std::string word);
        bool contains(std::string word);
        int nbWords();
        std::string toString(std::string tmp);
        bool isEndWord(int index);
        void setEndWord(int index, bool endWord);
        char getLetter(int index);
        void setLetter(char c);
    private:
        char* nodeList;
        bool* endWord;
        Dictionary** subDictionary;
};

#endif // DICTIONARY_H
