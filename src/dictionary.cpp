#include "dictionary.h"

Dictionary::Dictionary()
{
    nodeList = new char[LENGTH];
    endWord = new bool[LENGTH];
    subDictionary = new Dictionary*[26]();
}

void Dictionary::add(std::string word){
    char c = word.at(0);
    int pos = (int)c - (int)'a';
    if(subDictionary[pos] == nullptr){
        nodeList[pos] = c;
        endWord[pos] = false;
        subDictionary[pos] = new Dictionary();
    }
    if(word.length() == 1)
        endWord[pos] = true;
    else
        subDictionary[pos]->add(word.substr(1));
}

bool Dictionary::contains(std::string word){
    char c = word.at(0);
    int pos = (int)c - (int)'a';
    if(subDictionary[pos] == nullptr)
        return false;
    if(word.length() == 1)
        return endWord[pos];
    return subDictionary[pos]->contains(word.substr(1));
}

int Dictionary::nbWords(){
    int n = 0;
    for(int i = 0; i < LENGTH; i++){
        if(subDictionary[i] != nullptr){
            int m = 0;
            if(endWord[i])
                m++;
            return m+subDictionary[i]->nbWords();
        }
    }
    return n;
}

std::string Dictionary::toString(std::string tmp){
    std::string str = "";
    std::string cur = tmp;
    for(int i = 0; i < LENGTH; i++){
        if(subDictionary[i] != nullptr){
            cur += nodeList[i];
            if(endWord[i]){
                str += cur;
                str += '\n';
            }
            str += subDictionary[i]->toString(cur);
            cur = cur.substr(0, cur.length()-1);
        }
    }

    return str;
}
