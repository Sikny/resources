#include "bst.h"

template <typename T>
BST<T>::BST(){}

/**
 * @param value : value to add
 * @brief adds value in binary tree
 */
template <typename T>
void BST<T>::add(T value){
    if(this.value == nullptr) {
        this.value = value;
        leftTree = new BST();
        rightTree = new BST();
    }
    else if(value < this.value)
        rightTree.add(value);
    else
        leftTree.add(value);
}

/**
 * @param value : value to search in tree
 * @return true if value is in tree, else false
 * @brief searchs for presence of value in tree
 */
template <typename T>
bool BST<T>::contains(T value){
    if(this.value == nullptr)
        return false;
    if(this.value == value)
        return true;
    if(value < this.value)
        return rightTree.contains(value);
    if(value > this.value)
        return leftTree.contains(value);
    return false;
}
